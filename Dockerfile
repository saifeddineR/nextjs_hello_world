FROM node:17-alpine3.12
WORKDIR /app
COPY package.json ./
RUN npm install 
COPY . .
ENV PORT=5000
EXPOSE 8080
CMD ['npm','start']